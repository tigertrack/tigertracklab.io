import { Container, Navbar, Nav } from "react-bootstrap";
import Link from 'next/link'
export default function Default({children}){
    const NavNextLink = ({to, text}) => {
        return (
            <>
            <Link href={to} passHref>
                <Nav.Link>{text}</Nav.Link>
            </Link>
            </>
        )
    }
    return (
        <>
        <Navbar sticky="top" variant="dark" bg="dark" expand="lg">
            <Container  className=" py-2">
                <Link href="#" passHref>
                    <Navbar.Brand >@Tigertack</Navbar.Brand>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <NavNextLink to="#about" text="About" />
                        
                        <NavNextLink to="#experience" text="Experience" />
                        <NavNextLink to="#contact" text="Contact" />
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        <main >
            {children}
        </main>
        </>
    )
}