import Head from 'next/head'
import {
    Navbar,
    Container,
    Nav,
    NavDropdown,
    Row,
    Col
} from 'react-bootstrap'


export default function Home() {
    const styles = {
        fillMinusNavbar : {
            height: 'calc(100vh - 72px)'
        },
        hr: {
            borderStyle: 'solid',
            borderColor: '#8540f5',
            borderWidth: '5px',
            opacity: 1,
            
        },
    }
    const ListItem = ({title, status, company, duration}) => {
        return (
            // <li>
            //     <h4>
            //         {title}
            //         <span className="text-muted">{status}</span>
            //     </h4>
            //     <p>{company}</p>
            //     <p>{duration}</p>
            // </li>
            <div className="mb-5">
                <div className="d-flex justify-content-between">
                    <p className="h4">{company}</p>
                    <p className="h4">{duration}</p>
                </div>
                <p className="h5 text-muted">{status} - {duration}</p>
            </div>
        )
    }
    return (
        <div >

            <div className="bg-dark">
            <Container  className="text-light">
                <div
                    style={styles.fillMinusNavbar}
                    className="d-flex flex-column justify-content-center align-items-center">
                    <h1 className="">
                        <span className="text-muted">{"Hi, i'm "}</span>
                        <strong>{"Riandy Fadly"}</strong>
                    </h1>
                    <h3 className="text-center">{"Fullstack Developer & Bootcamp Facilitator"}</h3>
                </div>
            </Container>
            </div>

            <section id="about" className="bg-dark text-light" style={{paddingTop: '100px'}}>
                <Container style={styles.fillMinusNavbar}>
                    <Row>
                        <Col sm="12" md="6">
                            <h2 className="mb-5" style={{fontSize:"50px"}}>
                                About Me
                            </h2>
                            <h5 className="lh-lg " style={{color: '#8540f5'}}>
                                {"<p>"} <br />
                                <span className="d-inline-block font-monospace ps-5">
                                {"Well-seasoned web developer with a keen eye on performance and details. Proven experience in both Backend and Frontend development using Laravel and Vue.js as my main weapon. I've been maintaining multiple projects ranging from CLI-based Bot, multiple SPA websites and a cross-platform API Service. Currently exploring the field of Cloud Computing and Computer Vision."}
                                </span>
                                <br/> {"</p>"}
                            </h5>
                        </Col>
                        <Col sm="12" md="6">
                            <hr style={styles.hr} />
                        </Col>
                    </Row>
                </Container>
            </section>

            <section id="experience" className="bg-dark" style={{paddingTop: '100px'}}>
            <Container style={styles.fillMinusNavbar} className="text-light">
                <Row>
                    <Col sm="12" md="6">
                        <h2 className="mb-5" style={{fontSize:"50px"}}>
                            Place of Works
                        </h2>
                        
                    </Col>
                    <Col sm="12" md="6">
                        <hr className="my-5" style={styles.hr}/>
                        <ul className="list-unstyled pt-3">
                            <ListItem
                                title="Full Stack Web Facilitator"
                                company="Binar Academy"
                                status="Contract"
                                duration="Oct 2020 - Present"/>
                            <ListItem
                                title="Full Stack Engineer"
                                company="Budi Luhur University"
                                status="Full Time"
                                duration="Nov 2017 - Present"/>
                            <ListItem
                                title="Java Developer"
                                company="PT Cahaya Multitran Abadi"
                                status="Intern"
                                duration="Jan - May 2013"/>
                            <ListItem
                                title="PHP Developer"
                                company="PT Telkom Indonesia"
                                status="Intern"
                                duration="Jul - Sep 2013"/>

                        </ul>
                    </Col>

                </Row>
            </Container>
            </section>
        </div>
    )
}
